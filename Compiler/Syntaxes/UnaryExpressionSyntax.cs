﻿namespace MathExpreParser.Compiler.Syntaxes
{
    using System.Collections.Generic;

    namespace MathExpreParser.Compiler.Syntaxes
    {
        class UnaryExpressionSyntax : ExpressionSyntax
        {

            //There are two types of mathematical operators: unary and binary.Unary operators perform an
            //    action with a single operand.Binary operators perform actions with two operands.
            //    In a complex expression, (two or more operands) the order of evaluation depends on precedence rules.
            public UnaryExpressionSyntax(SyntaxToken operatorToken, ExpressionSyntax operand)
            {
                OperatorToken = operatorToken;
                Operand = operand;
            }

            public override SyntaxKind Kind => SyntaxKind.UnaryExpression;

            public SyntaxToken ExpressionToken
            {
                get;
            }
            public SyntaxToken OperatorToken
            {
                get;
            }
            public ExpressionSyntax Operand
            {
                get;
            }

            public override IEnumerable<SyntaxNode> GetChildren()
            {
                yield return OperatorToken;
                yield return Operand;
            }
        }
    }

}
