﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathExpreParser.Compiler.Syntaxes
{
    internal class ParenthesizedExpressionSyntax : ExpressionSyntax
    {
        public ParenthesizedExpressionSyntax(SyntaxToken openParenthesisToken, ExpressionSyntax expression, SyntaxToken closedParenthesisToken)
        {
            OpenParenthesisToken = openParenthesisToken;
            Expression = expression;
            ClosedParenthesisToken = closedParenthesisToken;
        }

        public override SyntaxKind Kind => SyntaxKind.ParenthesizedExpression;

        public SyntaxToken OpenParenthesisToken
        {
            get;
        }
        public ExpressionSyntax Expression
        {
            get;
        }
        public SyntaxToken ClosedParenthesisToken
        {
            get;
        }

        public override IEnumerable<SyntaxNode> GetChildren()
        {
            //    The yield return statement returns one element at a time. The return type of yield keyword is either IEnumerable or IEnumerator.The yield break statement 
            //        is used to end the iteration.We can consume the iterator method that contains a yield return statement either by using foreach loop or LINQ query.
            yield return OpenParenthesisToken;
            yield return Expression;
            yield return ClosedParenthesisToken;
        }
    }
}
