﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathExpreParser.Compiler.Syntaxes
{
    class BinaryExpressionSyntax : ExpressionSyntax
    {
        public BinaryExpressionSyntax(ExpressionSyntax left, SyntaxToken operatorToken, ExpressionSyntax right)
        {
            this.ExpressionToken = ExpressionToken;
            Left = left;
            OperatorToken = operatorToken;
            Right = right;
        }

        public override SyntaxKind Kind => SyntaxKind.BinaryExpression;

        public SyntaxToken ExpressionToken
        {
            get;
        }
        public ExpressionSyntax Left
        {
            get;
        }
        public SyntaxToken OperatorToken
        {
            get;
        }
        public ExpressionSyntax Right
        {
            get;
        }

        public override IEnumerable<SyntaxNode> GetChildren()
        {
            yield return Left;
            yield return OperatorToken;
            yield return Right;
        }
    }
}
