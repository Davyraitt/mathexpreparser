﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathExpreParser.Compiler.Syntaxes
{

    //SyntraxTree will hold on to our expression 
    internal class SyntaxTree
    {
        public SyntaxTree(IEnumerable<string> diagnostics, ExpressionSyntax root, SyntaxToken endOfFileToken)
        {
            Diagnostics = diagnostics.ToArray();
            Root = root;
            EndOfFileToken = endOfFileToken;
        }

        public static SyntaxTree Parse(string text)
        {
            var parser = new Parser(text);
            return parser.Parse();
        }

        //The IReadOnlyList<T> represents a list in which the number and order of list elements is read-only.The content of list elements is not guaranteed to be read-only.
        public IReadOnlyList<string> Diagnostics
        {
            get;
        }
        public ExpressionSyntax Root
        {
            get;
        }
        public SyntaxToken EndOfFileToken
        {
            get;
        }
    }
}
