﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathExpreParser.Compiler.Syntaxes
{

    //A Literal Expression is the simplest form of DMN expression; it is commonly defined as a single-line statement,
    //    or an if-else conditional block.The Literal Expression is a type of value expression used in both Decision elements and Business Knowledge Model (BKM) elements.
    internal class LiteralExpressionSyntax : ExpressionSyntax
    {
        public LiteralExpressionSyntax(SyntaxToken literalToken)
        {
            LiteralToken = literalToken;
        }

        public override SyntaxKind Kind => SyntaxKind.LiteralExpression;

        public SyntaxToken LiteralToken
        {
            get;
        }

        public override IEnumerable<SyntaxNode> GetChildren()
        {
            yield return LiteralToken;
        }
    }
}
