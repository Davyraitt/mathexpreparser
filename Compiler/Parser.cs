﻿using MathExpreParser.Compiler.Syntaxes;
using MathExpreParser.Compiler.Syntaxes.MathExpreParser.Compiler.Syntaxes;
using System.Collections.Generic;

namespace MathExpreParser.Compiler
{
    internal class Parser
    {
        private readonly SyntaxToken[] _tokens;
        private int _position;
        private List<string> _diagnostics = new List<string>();


        public Parser(string text)
        {
            var tokens = new List<SyntaxToken>();
            var lexer = new Lexer(text);

            SyntaxToken token;

            do
            {
                token = lexer.Lex();

                if (token.Kind != SyntaxKind.WhiteSpaceToken
                    &&
                    token.Kind != SyntaxKind.BadToken
                    )
                {
                    tokens.Add(token);
                }
            } while (token.Kind != SyntaxKind.EndOfFileToken);

            _tokens = tokens.ToArray();

            //Add diagnostics from lexer so we dont forget what the lexer reported!
            _diagnostics.AddRange(lexer.Diagnostics);
        }

        //IEnumerable is an interface defining a single method GetEnumerator()
        //that returns an IEnumerator interface. It is the base interface for all non-generic collections that can be enumerated.
        public IEnumerable<string> Diagnostics => _diagnostics;

        private SyntaxToken Check(int offset)
        {
            var index = _position + offset; // index we arrive at with current position and given offset
            if (index >= _tokens.Length)
            {
                return _tokens[_tokens.Length - 1];
            }

            else
            {
                return _tokens[index];
            }
        }

        private SyntaxToken Current => Check(0);

        private SyntaxToken NextToken()
        {
            var current = Current;
            _position++;
            return current;
        }

        private SyntaxToken MatchToken(SyntaxKind kind)
        {
            if (Current.Kind == kind)
            {
                return NextToken();
            }

            else
            {
                _diagnostics.Add("Error: Unexpected token of kind " + Current.Kind + " We expected " + kind);
                return new SyntaxToken(kind, Current.Position, null, null);
            }
        }

        public ExpressionSyntax ParseExpression(int parentPriority = 0)
        {
            ExpressionSyntax left;

            var unaryOperatorPriority = GetUnaryOperatorPriority(Current.Kind);

            if (unaryOperatorPriority != 0 && unaryOperatorPriority >= parentPriority)
            {
                var operatorToken = NextToken();
                var operand = ParseExpression(unaryOperatorPriority);
                left = new UnaryExpressionSyntax(operatorToken, operand);
            }
            else
            {
                left = ParsePrimaryExpression();

            }

            while (true)
            {
                var priority = GetBinaryOperatorPriority(Current.Kind);

                if (priority == 0 || priority <= parentPriority)
                {
                    break;
                }

                else
                {
                    var operatorToken = NextToken();
                    var right = ParseExpression(priority);
                    left = new BinaryExpressionSyntax(left, operatorToken, right);
                }


            }

            return left;


        }


        //pass in a SyntaxKind and from the kind we retrieve the priority
        public static int GetBinaryOperatorPriority(SyntaxKind kind)
        {

            //PEMDAS:
            //    Parentheses, Exponents, Multiplication and Division(from left to right), Addition and Subtraction(from left to right).
            switch (kind)
            {
                case SyntaxKind.MultiplierToken:
                    return 2;
                case SyntaxKind.SlashToken:
                    return 2;
                case SyntaxKind.PlusToken:
                    return 1;
                case SyntaxKind.MinusToken:
                    return 1;
                default:
                    return 0;
            }
        }

        public static int GetUnaryOperatorPriority(SyntaxKind kind)
        {

            //PEMDAS:
            //    Parentheses, Exponents, Multiplication and Division(from left to right), Addition and Subtraction(from left to right).
            //PEMDAS:
            //    Parentheses, Exponents, Multiplication and Division(from left to right), Addition and Subtraction(from left to right).
            switch (kind)
            {
                case SyntaxKind.PlusToken:
                    return 3;
                case SyntaxKind.MinusToken:
                    return 3;
                default:
                    return 0;
            }

        }


        public SyntaxTree Parse()
        {
            var expression = ParseExpression();
            var endOfFileToken = MatchToken(SyntaxKind.EndOfFileToken);

            return new SyntaxTree(_diagnostics, expression, endOfFileToken);
        }

        private ExpressionSyntax ParsePrimaryExpression()
        {
            if (Current.Kind == SyntaxKind.OpenParenthesisToken)
            {
                var left = NextToken();
                var expression = ParseExpression();

                var right = MatchToken(SyntaxKind.ClosedParenthesisToken);

                return new ParenthesizedExpressionSyntax(left, expression, right);
            }


            var numberToken = MatchToken(SyntaxKind.NumberToken);
            return new LiteralExpressionSyntax(numberToken);
        }
    }
}
