﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace MathExpreParser.Compiler
{

    enum SyntaxKind
    {
        //Special Tokens
        NumberToken,
        WhiteSpaceToken,
        BadToken,
        EndOfFileToken,

        //Operators
        PlusToken,
        MinusToken,
        MultiplierToken,
        SlashToken,
        OpenParenthesisToken,
        ClosedParenthesisToken,

        //Expressions
        LiteralExpression,
        BinaryExpression,
        ParenthesizedExpression,
        UnaryExpression
    }

    internal class SyntaxToken : SyntaxNode
    {

        public SyntaxToken(SyntaxKind kind, int position, string text, object value)
        {
            Kind = kind;
            Position = position;
            Text = text;
            Value = value;
        }

        public override SyntaxKind Kind
        {
            get;
        }
        public int Position
        {
            get;
        }
        public string Text
        {
            get;
        }
        public object Value
        {
            get;
        }

        public override IEnumerable<SyntaxNode> GetChildren()
        {
            return Enumerable.Empty<SyntaxNode>();
        }
    }
}
