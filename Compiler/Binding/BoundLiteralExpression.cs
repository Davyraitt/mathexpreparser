﻿using System;

namespace MathExpreParser.Compiler.Binding
{
    internal class BoundLiteralExpression : BoundExpression
    {
        public BoundLiteralExpression(object value)
        {
            Value = value;
        }

        public override Type Type => Value.GetType();

        public override BoundNodeKind Kind => BoundNodeKind.LiteralExpression;

        public object Value
        {
            get;
        }
    }
}
