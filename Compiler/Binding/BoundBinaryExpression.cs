﻿using System;

namespace MathExpreParser.Compiler.Binding
{
    internal class BoundBinaryExpression : BoundExpression
    {

        internal enum BoundBinaryOperatorKind
        {
            Addition,
            Subtraction,
            Multiplication,
            Division
        }

        public BoundBinaryExpression(BoundExpression left, BoundBinaryOperatorKind operatorKind, BoundExpression right)
        {
            Left = left;
            Right = right;
        }

        public override Type Type => Left.Type;

        public override BoundNodeKind Kind => BoundNodeKind.BinaryExpression;

        public BoundExpression Left
        {
            get;
        }
        public BoundBinaryOperatorKind OperatorKind
        {
            get;
        }
        public BoundExpression Right
        {
            get;
        }
        public BoundExpression Operand
        {
            get;
        }
    }
}
