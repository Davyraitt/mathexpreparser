﻿namespace MathExpreParser.Compiler.Binding
{
    internal enum BoundNodeKind
    {
        UnaryExpression,
        LiteralExpression,
        BinaryExpression
    }

    internal abstract class BoundNode
    {
        public abstract BoundNodeKind Kind
        {
            get;
        }
    }
}
