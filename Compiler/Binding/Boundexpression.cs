﻿using System;

namespace MathExpreParser.Compiler.Binding
{
    internal abstract class BoundExpression : BoundNode
    {
        public abstract Type Type
        {
            get;
        }
    }
}
