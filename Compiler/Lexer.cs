﻿
using System.Collections.Generic;

namespace MathExpreParser.Compiler
{
    internal class Lexer
    {

        private readonly string _text;
        private int _position;
        private List<string> _diagnostics = new List<string>();

        public Lexer(string text)
        {
            this._text = text;
        }


        //IEnumerable is an interface defining a single method GetEnumerator()
        //that returns an IEnumerator interface. It is the base interface for all non-generic collections that can be enumerated.
        public IEnumerable<string> Diagnostics => _diagnostics;

        private char Current
        {
            get
            {
                if (_position >= _text.Length) //outside bounds of text
                {
                    return '\0'; // zero terminator, escape sequence
                }

                return _text[_position];
            }
        }

        private void Next()
        {
            _position++;
        }


        //Represents a token in the syntax tree.
        //When Lex gets called we are at a position for a file, and then find the next token
        public SyntaxToken Lex()
        {

            //To perform lexical analysis;
            //to convert a character stream to a token stream as a preliminary to parsing.

            if (_position >= _text.Length) // end of the file/text, no more data coming
            {
                return new SyntaxToken(SyntaxKind.EndOfFileToken, -_position, "\0", null);
            }


            if (char.IsDigit(Current)) // if character is a digit
            {
                var start = _position;

                while (char.IsDigit(Current))
                {
                    Next();
                    var length = _position - start;
                    var text = _text.Substring(start, length);


                    if (!int.TryParse(text, out var value))
                    {
                        _diagnostics.Add("The number " + _text + " can not become a Int");
                    }

                    return new SyntaxToken(SyntaxKind.NumberToken, start, text, value);
                }
            }

            if (char.IsWhiteSpace(Current)) //if character is a whitespace
            {
                var start = _position;

                while (char.IsWhiteSpace(Current))
                {
                    Next();
                    var length = _position - start;
                    var text = _text.Substring(start, length);

                    return new SyntaxToken(SyntaxKind.WhiteSpaceToken, start, text, null);
                }

            }

            switch (Current)
            {
                case '+':
                    return new SyntaxToken(SyntaxKind.PlusToken, -_position++, "+", null);
                case '-':
                    return new SyntaxToken(SyntaxKind.MinusToken, -_position++, "-", null);
                case '*':
                    return new SyntaxToken(SyntaxKind.MultiplierToken, -_position++, "*", null);
                case '/':
                    return new SyntaxToken(SyntaxKind.SlashToken, -_position++, "/", null);
                case '(':
                    return new SyntaxToken(SyntaxKind.OpenParenthesisToken, -_position++, "(", null);
                case ')':
                    return new SyntaxToken(SyntaxKind.ClosedParenthesisToken, -_position++, ")", null);
            }

            _diagnostics.Add("Error, bad character.." + Current);

            //nothing from our "verzameling!"
            return new SyntaxToken(SyntaxKind.BadToken, -_position++, _text.Substring(_position - 1, 1), null);

        }
    }
}
