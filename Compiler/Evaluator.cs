﻿using MathExpreParser.Compiler.Binding;
using System;

namespace MathExpreParser.Compiler
{
    internal class Evaluator
    {
        private readonly BoundExpression _root;

        public Evaluator(BoundExpression root)
        {
            _root = root;
        }

        public int Evaluate()
        {
            return EvaluateExpression(_root);
        }

        private int EvaluateExpression(BoundExpression node)
        {
            // We now have:

            //BinaryExpression
            //NumberExpression

            if (node is BoundLiteralExpression n)
            {
                //We can safely cast, because in the Parser we check if it is an int
                return (int)n.Value;
            }

            if (node is BoundUnaryExpression u)
            {
                var operand = EvaluateExpression(u.Operand);

                switch (u.OperatorKind)
                {
                    case BoundUnaryOperatorKind.Adding:
                        return operand;
                    case BoundUnaryOperatorKind.Negation:
                        return -operand;
                    default:
                        throw new Exception("Unexpected unary operator, of kind : " + u.OperatorKind);
                }
            }

            if (node is BoundBinaryExpression b)
            {
                var left = EvaluateExpression(b.Left);
                var right = EvaluateExpression(b.Right);

                //We got our left and right, now there should be a Binary Expression in the middle :)

                // +
                switch (b.OperatorKind)
                {
                    case BoundBinaryExpression.BoundBinaryOperatorKind.Addition:
                        return left + right;
                    case BoundBinaryExpression.BoundBinaryOperatorKind.Subtraction:
                        return left - right;
                    case BoundBinaryExpression.BoundBinaryOperatorKind.Multiplication:
                        return left * right;
                    case BoundBinaryExpression.BoundBinaryOperatorKind.Division:
                        return left / right;
                    default:
                        throw new Exception("Unexpected binary operator, of kind : " + b.OperatorKind);
                }
            }




            throw new Exception("Unexpected binary operator, of kind : " + node.Kind);
        }
    }
}
