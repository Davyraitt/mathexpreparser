﻿using MathExpreParser.Compiler;
using MathExpreParser.Compiler.Binding;
using MathExpreParser.Compiler.Syntaxes;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace MathExpreParser.MVVM.View
{
    /// <summary>
    /// Interaction logic for HomeView.xaml
    /// </summary>
    public partial class HomeView : UserControl
    {

        //first part of compiler is the lexer, which creates tokens.
        //the lexer performs a lexical analysis, and seperates the string entered to a stream of different numbers/operators
        //the parser then scans the tokens and produces the parsing result.


        //n computer science, a recursive descent parser is a kind of top-down parser built from a set of
        //mutually recursive procedures(or a non-recursive equivalent) where each such procedure implements one of the nonterminals of the grammar.

        public HomeView()
        {
            InitializeComponent();
        }

        private void CalculateClick(object sender, RoutedEventArgs e)
        {
            string input = InvoerTextBox.Text;

            if (input == null || input.Length == 0)
            {
                logToLogBox("Enter a valid string!");
            }

            else
            {

                textBlockLogging.Text = "";
                textBlockVisualisatie.Text = "";
                logToLogBox("Input is: " + input);
                Calculate(input);
            }

        }

        private void Calculate(string input)
        {
            var lexer = new Lexer(input);
            var parser = new Parser(input);
            var syntaxTree = SyntaxTree.Parse(input);
            var binder = new Binder();
            var boundExpression = binder.BindExpression(syntaxTree.Root);
            IReadOnlyList<string> diagnostics = syntaxTree.Diagnostics;
            var diagnosticsList = syntaxTree.Diagnostics.Concat(binder.Diagnostics).ToArray();


            //Parser printing
            Print(syntaxTree.Root);


            //Lexer Printing

            while (true)
            {
                var token = lexer.Lex();

                if (token.Kind == SyntaxKind.EndOfFileToken)
                {
                    logToLogBox("---End of lexer log---");
                    break;
                }

                logToLogBox("|K " + token.Kind + "      |" + token.Text + "|" + "| V   " + token.Value);

            }

            //Error printing

            if (!diagnosticsList.Any())
            {

                var e = new Evaluator(boundExpression);
                var result = e.Evaluate();

                logToResBox(result.ToString());

            }

            //If no errors, we can calculate the string
            else
            {
                foreach (var diagnostic in syntaxTree.Diagnostics)
                {
                    logToLogBox(diagnostic);
                }
            }

        }

        private void logToLogBox(string textinput)
        {
            textBlockLogging.Text = textBlockLogging.Text + System.Environment.NewLine + textinput;
        }

        private void logToVisBox(string textinput)
        {
            textBlockVisualisatie.Text = textBlockVisualisatie.Text + System.Environment.NewLine + textinput;
        }

        private void logToResBox(string textinput)
        {
            textBlockResult.Text = textinput;
        }

        private void Print(SyntaxNode node, string indent = "", bool isLast = true)
        {

            //└──NumberToken 1
            //├──PlusToken
            //
            // │ 

            //When parsing the expression 1 + 2 * 3 we need to parse it into a tree structure that honors priorties, i.e. that * binds stronger than +:

            //        └──BinaryExpression
            //├──NumberExpression
            //│   └──NumberToken 1
            //├──PlusToken
            //└──BinaryExpression
            //    ├──NumberExpression
            //    │   └──NumberToken 2
            //    ├──StarToken
            //    └──NumberExpression
            //        └──NumberToken 3



            string nodeKind = node.Kind.ToString();
            var marker = isLast ? "└──" : "├──";

            if (node is SyntaxToken t && t.Value != null)
            {
                string nodeValue = t.Value.ToString();
                logToVisBox(indent + marker + nodeKind + " " + nodeValue);
            }
            else
            {
                logToVisBox(indent + marker + nodeKind);

            }


            indent += isLast ? "    " : "│   ";

            var lastChild = node.GetChildren().LastOrDefault();

            foreach (var child in node.GetChildren())
            {
                Print(child, indent, child == lastChild);
            }
        }

    }
}
