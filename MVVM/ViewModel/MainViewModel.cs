﻿using MathExpreParser.Core;

namespace MathExpreParser.MVVM.ViewModel
{
    internal class MainViewModel : ObservableObject
    {
        public RelayCommand HomeViewCommand
        {
            get; set;
        }

        public RelayCommand LogViewCommand
        {
            get; set;
        }

        public HomeViewModel HomeVM
        {
            get; set;
        }

        public LoggingViewModel LogVM
        {
            get; set;
        }

        private object _currentView;

        public object CurrentView
        {
            get
            {
                return _currentView;
            }
            set
            {
                _currentView = value;
                OnPropertyChanged();
            }
        }


        public MainViewModel()
        {
            HomeVM = new HomeViewModel();
            LogVM = new LoggingViewModel();
            CurrentView = HomeVM;

            HomeViewCommand = new RelayCommand(o =>
            {
                CurrentView = HomeVM;
            });

            LogViewCommand = new RelayCommand(o =>
            {
                CurrentView = LogVM;
            });
        }
    }
}
